import com.filmrental.Film;
import com.filmrental.NewFilm;
import com.filmrental.OldFilm;
import com.filmrental.RegularFilm;

public class Main {
  public static void main(String[] args) {
    Film f1 = new Film("Matrix 11", new NewFilm());
    Film f2 = new Film("Spider man", new RegularFilm());
    Film f3 = new Film("Spider man 2", new RegularFilm());
    Film f4 = new Film("Out of Africa", new OldFilm());

    InventoryImpl inv = new InventoryImpl();
    inv.addFilm(f1, 1);
    inv.addFilm(f2, 2);
    inv.addFilm(f3, 3);
    inv.addFilm(f4, 1);

    User u1 = new User(2);

    Rental r = new Rental(inv, u1);
    r.addFilmToRental(f1, 1);
    r.addFilmToRental(f2, 5);
    r.addFilmToRental(f3, 2);
    r.addFilmToRental(f4, 7);
    System.out.println();

    System.out.println("All films: " + inv.getAllFilms());
    System.out.println("Films in store: " + inv.getAllFilmsInStore());
    System.out.println();

    r.rentAll(true);
    System.out.println();

    r.returnFilm(f1, 2);
    r.returnFilm(f2, 5);
    r.returnFilm(f3, 1);
    r.returnFilm(f4, 9);
    System.out.println();

    System.out.println("All films: " + inv.getAllFilms());
    System.out.println("Films in store: " + inv.getAllFilmsInStore());
  }
}
