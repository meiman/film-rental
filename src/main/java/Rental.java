import com.filmrental.Film;
import com.filmrental.NewFilm;

import java.util.HashMap;
import java.util.Map;

public class Rental {
  private Map<Film, Integer> filmsAndDays;
  private final InventoryImpl inventory;
  private final User user;

  public Rental(InventoryImpl inventory, User user) {
    this.inventory = inventory;
    this.user = user;
    filmsAndDays = new HashMap<>();
  }

  public Map<Film, Integer> getFilmsAndDays() {
    return filmsAndDays;
  }

  public boolean addFilmToRental(Film film, int amountOfDays) {
    if (inventory.rentFilm(film))  {
      filmsAndDays.put(film, amountOfDays);
      System.out.println("Film " + film.toString() + " added to rental");
      return true;
    } else {
      System.out.println("Film is not in inventory");
      return false;
    }
  }

  public int returnFilm(Film film, int amountOfDays)  {
    if (filmsAndDays.keySet().contains(film) && amountOfDays>=0) {
      inventory.returnFilm(film);
      int daysOverDeadline = amountOfDays-filmsAndDays.get(film);
      if (daysOverDeadline>0) {
        // price for only extra days
        int extraPrice = film.getRentalPrice(amountOfDays)-film.getRentalPrice(filmsAndDays.get(film));
        filmsAndDays.remove(film);
        return payForFilm(film, daysOverDeadline, extraPrice, false, true);
      } else {
        System.out.println(film.toString()+ " returned");
        filmsAndDays.remove(film);
        return 0;
      }
    } else {
      System.out.println("Invalid input");
      return -1;
    }
  }

  private int payForFilm(Film film, int days, int price, boolean useBonus, boolean isExtra)  {
    boolean printBonus = false;
    int bonusPointsPerNewFilm = 25;
    if (useBonus && film.getType() instanceof NewFilm && user.getBonusPoints()>= bonusPointsPerNewFilm) {
      user.changeBonusPoints(-bonusPointsPerNewFilm);
      printBonus = true;
      price = 0;
    }
    System.out.println(formatRentalReceiptLine(film, days, price, printBonus, isExtra));
    return price;
  }

  public int rentAll(boolean useBonus) {
    int total = 0;
    int bonusPointsGained = getBonusPointsFromRental();
    for (Map.Entry<Film, Integer> filmAndDay : filmsAndDays.entrySet()) {
      total+=payForFilm(filmAndDay.getKey(), filmAndDay.getValue(), filmAndDay.getKey().getRentalPrice(filmAndDay.getValue()), useBonus, false);
    }
    user.changeBonusPoints(bonusPointsGained);
    System.out.println("Total: " + total + " EUR");
    System.out.println("Remaining bonuspoints: " + (user.getBonusPoints()));
    return total;
  }

  public int getBonusPointsFromRental() {
    return filmsAndDays.keySet().stream().mapToInt(this::bonusPointsPerFIlm).sum();
  }

  private int bonusPointsPerFIlm(Film film) {
    if (film.getType() instanceof NewFilm) return 2;
    else return 1;
  }

  private String formatRentalReceiptLine(Film film, int days, int price, boolean bonusPayment, boolean isExtra) {
    StringBuilder sb = new StringBuilder();
    sb.append(film.toString());
    if (isExtra) sb.append(" extra");
    sb.append(" ").append(days).append(" days ").append(price).append(" EUR");
    if (bonusPayment) sb.append("(payed with bunuspoints)");
    return sb.toString();
  }
}
