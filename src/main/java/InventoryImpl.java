import com.filmrental.Film;
import com.filmrental.FilmType;

import java.util.*;

public class InventoryImpl implements Inventory {
  private Map<Film, Integer>  filmsWithQuantity = new HashMap<>();

  public InventoryImpl() { }

  public boolean rentFilm(Film filmToRent)  {
    if (filmToRent != null && filmsWithQuantity.get(filmToRent)>0) {
      changeFilmQuantity(filmToRent, filmsWithQuantity.get(filmToRent)-1);
      return true;
    }
    return false;
  }

  public boolean returnFilm(Film filmToReturn) {
    if (filmToReturn != null && filmsWithQuantity.containsKey(filmToReturn)) {
      changeFilmQuantity(filmToReturn, filmsWithQuantity.get(filmToReturn)+1);
      return true;
    }
    return false;
  }

  public Film getFilmByTitle(String title)  {
    for (Film film : getAllFilms().keySet()) {
      if (film.getTitle().equals(title))  {
        return film;
      }
    }
    return null;
  }

  public void changeFilmQuantity(Film film, int newQuantity)  {
    if (film != null && newQuantity>=0) filmsWithQuantity.replace(film, newQuantity);
  }

  public int getFilmQuantity(Film film)  {
    return filmsWithQuantity.get(film);
  }

  @Override
  public void addFilm(Film film, int quantity) {
    filmsWithQuantity.put(film, quantity);
  }

  @Override
  public boolean removeFilm(Film film) {
    if (!filmsWithQuantity.containsKey(film)) return false;

    filmsWithQuantity.remove(film);
    return true;
  }

  @Override
  public boolean changeFilmType(Film film, FilmType newType) {
    if (filmsWithQuantity.keySet().contains(film))  {
      int quantity = filmsWithQuantity.get(film);
      filmsWithQuantity.remove(film);
      film.setType(newType);
      filmsWithQuantity.put(film, quantity);
      return true;
    }
    return false;
  }

  @Override
  public Map<Film, Integer> getAllFilms() {
    return filmsWithQuantity;
  }

  @Override
  public Map<Film, Integer> getAllFilmsInStore() {
    Map<Film, Integer> out = new HashMap<>();
    filmsWithQuantity.entrySet().stream().filter(e -> e.getValue()>0).forEach(e -> out.put(e.getKey(), e.getValue()));
    return out;
  }
}
