import com.filmrental.Film;
import com.filmrental.FilmType;

import java.util.Map;

public interface Inventory {
  void addFilm(Film film, int quantity);

  boolean removeFilm(Film film);

  boolean changeFilmType(Film film, FilmType newType);

  Map<Film, Integer> getAllFilms();

  Map<Film, Integer> getAllFilmsInStore();
}
