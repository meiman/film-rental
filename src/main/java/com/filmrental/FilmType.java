package com.filmrental;


public abstract class FilmType {
  public abstract int getRentalPrice(int days);
}
