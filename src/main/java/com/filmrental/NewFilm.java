package com.filmrental;

public class NewFilm extends FilmType {
  @Override
  public int getRentalPrice(int days) {
    return Film.PREMIUM_PRICE * days;
  }

  @Override
  public String toString()  {
    return "New film";
  }
}
