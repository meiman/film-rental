package com.filmrental;

public class OldFilm extends FilmType {
  @Override
  public int getRentalPrice(int days) {
    if (days <= 5)  return Film.BASIC_PRICE;
    else  return Film.BASIC_PRICE + (days-5) * Film.BASIC_PRICE;
  }

  @Override
  public String toString()  {
    return "Old film";
  }
}
