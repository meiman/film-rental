package com.filmrental;

public class Film {
  static final int PREMIUM_PRICE = 4;
  static final int BASIC_PRICE = 3;

  private final String title;
  private FilmType type;

  public Film(String title, FilmType type)  {
    this.title = title;
    this.type = type;
  }

  public void setType(FilmType type) {
    this.type = type;
  }

  public FilmType getType() {
    return type;
  }

  public String getTitle() {
    return title;
  }

  public int getRentalPrice(int days) {
    if (days<=0) return 0;
    return type.getRentalPrice(days);
  }

  @Override
  public String toString()  {
    return title +
            "(" +
            type.toString() +
            ")";
  }

}
