package com.filmrental;

public class RegularFilm extends FilmType {
  @Override
  public int getRentalPrice(int days) {
    if (days <= 3)  return Film.BASIC_PRICE;
    else  return Film.BASIC_PRICE + (days-3) * Film.BASIC_PRICE;
  }

  @Override
  public String toString()  {
    return "Regular film";
  }
}
