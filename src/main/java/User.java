public class User {
  private int bonusPoints;

  public User(int bonusPoints) {
    this.bonusPoints = bonusPoints;
  }

  public int getBonusPoints() {
    return bonusPoints;
  }

  public void changeBonusPoints(long changeAmmount) {
    this.bonusPoints+=changeAmmount;
    if (bonusPoints<0) bonusPoints=0;
  }

  public void setBonusPoints(int bonusPoints) {
    if (bonusPoints>0)  this.bonusPoints = bonusPoints;
    else this.bonusPoints=0;
  }
}
