import com.filmrental.Film;
import com.filmrental.NewFilm;
import com.filmrental.OldFilm;
import com.filmrental.RegularFilm;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class InventoryImplTest {
  private final InventoryImpl inv = new InventoryImpl();

  private List<Film> films = new ArrayList<>(Arrays.asList(
          new Film("Alien", new OldFilm()),
          new Film("Jaws", new OldFilm()),
          new Film("Terminator", new RegularFilm()),
          new Film("Mullholland drive", new RegularFilm()),
          new Film("Fargo", new OldFilm()),
          new Film("Blade runner 2049", new NewFilm()),
          new Film("Baby driver", new NewFilm())
  ));

  @BeforeEach
  public void initInventory() {
    for (Film film : films) {
      inv.addFilm(film, 1);
    }
  }

  @Test
  public void testNulls() {
    assertFalse(inv.removeFilm(null));
    assertFalse(inv.changeFilmType(new Film("Fargo", new OldFilm()), null));
    assertFalse(inv.changeFilmType(null, new NewFilm()));
  }

  @Test
  public void testAddingFilms() {
    Map<Film, Integer> expected = new HashMap<>();

    for (Film film : films) {
      expected.put(film, 1);
    }

    assertEquals(inv.getAllFilms(), expected);
  }

  @Test
  public void testRemovingFilms()  {
    Film filmToTest = inv.getFilmByTitle("Fargo");

    assertTrue(inv.getAllFilms().keySet().contains(filmToTest));

    inv.removeFilm(filmToTest);
    assertFalse(inv.getAllFilms().keySet().contains(filmToTest));

    inv.removeFilm(filmToTest);
    assertFalse(inv.getAllFilms().keySet().contains(filmToTest));
  }

  @Test
  public void testChangeFilmType()  {
    String title = "Mullholland drive";
    Film filmToTest = inv.getFilmByTitle(title);

    boolean result = inv.changeFilmType(filmToTest, new OldFilm());
    assertTrue(result);
    assertTrue(filmToTest.getType() instanceof OldFilm);

    result = inv.changeFilmType(filmToTest, new RegularFilm());
    assertTrue(result);
    assertTrue(filmToTest.getType() instanceof RegularFilm);

    assertFalse(inv.changeFilmType(new Film("-", new NewFilm()), new OldFilm()));
  }

  @Test
  public void testChangeFilmQuantity()  {
    String title = "Alien";
    Film filmToTest = inv.getFilmByTitle(title);

    inv.changeFilmQuantity(filmToTest, 100);
    assertEquals(100, inv.getAllFilms().get(filmToTest).intValue());

    inv.changeFilmQuantity(filmToTest, 0);
    assertEquals(0, inv.getAllFilms().get(filmToTest).intValue());

    inv.changeFilmQuantity(filmToTest, -12);
    assertEquals(0, inv.getAllFilms().get(filmToTest).intValue());
  }

  @Test
  public void testRentFilm()  {
    String title = "Terminator";
    Film filmToTest = inv.getFilmByTitle(title);
    inv.changeFilmQuantity(filmToTest, 1);

    boolean result = inv.rentFilm(filmToTest);
    System.out.println(filmToTest);
    System.out.println(inv.getAllFilms());
    assertEquals(0, inv.getAllFilms().get(filmToTest).intValue());
    assertTrue(result);

    result = inv.rentFilm(filmToTest);
    assertEquals(0, inv.getAllFilms().get(filmToTest).intValue());
    assertFalse(result);

    inv.changeFilmQuantity(filmToTest, 1);
  }

  @Test
  public void testReturn()  {
    String title = "Terminator";
    Film filmToTest = inv.getFilmByTitle(title);
    int initialQuantity = inv.getAllFilms().get(filmToTest);

    boolean result = inv.returnFilm(filmToTest);
    assertTrue(result);
    assertEquals(initialQuantity+1, inv.getAllFilms().get(filmToTest).intValue());

    inv.removeFilm(filmToTest);
    assertFalse(inv.returnFilm(filmToTest));
  }
}
