import com.filmrental.Film;
import com.filmrental.NewFilm;
import com.filmrental.OldFilm;
import com.filmrental.RegularFilm;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class RentalTest {
  private final InventoryImpl inv = new InventoryImpl();
  private final User user = new User(20);

  @BeforeEach
  public void initInventory() {
    List<Film> films = new ArrayList<>(Arrays.asList(
            new Film("Alien", new OldFilm()),
            new Film("Jaws", new OldFilm()),
            new Film("Terminator", new RegularFilm()),
            new Film("Mullholland drive", new RegularFilm()),
            new Film("Fargo", new OldFilm()),
            new Film("Blade runner 2049", new NewFilm()),
            new Film("Baby driver", new NewFilm())
    ));

    for (Film film : films) {
      inv.addFilm(film, 1);
    }
  }

  @Test
  public void testNulls() {
    Rental rental = new Rental(inv, user);

    assertFalse(rental.addFilmToRental(null, 2));
    assertEquals(-1, rental.returnFilm(null, 2));
  }

  @Test
  public void testAddExistingFilmToRental() {
    Rental rental = new Rental(inv, user);
    String title = "Alien";
    Film film = inv.getFilmByTitle(title);
    int quantityBefore = inv.getFilmQuantity(film);

    boolean result = rental.addFilmToRental(film, 5);
    assertTrue(result);
    assertTrue(rental.getFilmsAndDays().containsKey(inv.getFilmByTitle(title)));
    assertEquals(quantityBefore-1, inv.getFilmQuantity(film));
  }

  @Test
  public void testAddMissingFilmToRental()  {
    Rental rental = new Rental(inv, user);
    String title = "Moonrise kingdom";
    Film film = inv.getFilmByTitle(title);

    boolean result = rental.addFilmToRental(film, 1);

    assertFalse(result);
    assertFalse(rental.getFilmsAndDays().containsKey(inv.getFilmByTitle(title)));
  }

  @Test
  public void testReturnRentedFilmInTime()  {
    Rental rental = new Rental(inv, user);
    String title = "Alien";
    Film film = inv.getFilmByTitle(title);
    int quantityBefore = inv.getFilmQuantity(film);

    rental.addFilmToRental(film, 5);

    int result = rental.returnFilm(film, 3);

    assertEquals(0, result);
    assertFalse(rental.getFilmsAndDays().containsKey(inv.getFilmByTitle(title)));
    assertEquals(quantityBefore, inv.getFilmQuantity(film));
  }

  @Test
  public void testReturnRentedFilmLate()  {
    Rental rental = new Rental(inv, user);
    String title = "Alien";
    Film film = inv.getFilmByTitle(title);
    int quantityBefore = inv.getFilmQuantity(film);
    rental.addFilmToRental(film, 2);

    int result = rental.returnFilm(film, 6);

    assertEquals(3, result);
    assertFalse(rental.getFilmsAndDays().containsKey(inv.getFilmByTitle(title)));
    assertEquals(quantityBefore, inv.getFilmQuantity(film));
  }

  @Test
  public void testReturnNotRentedFilm()  {
    Rental rental = new Rental(inv, user);
    String title = "Alien";
    Film film = inv.getFilmByTitle(title);

    int result = rental.returnFilm(film, 3);

    assertEquals(-1, result);
    assertFalse(rental.getFilmsAndDays().containsKey(inv.getFilmByTitle(title)));
  }

  @Test
  public void testNegativeDaysReturn()  {
    Rental rental = new Rental(inv, user);
    String title = "Alien";
    Film film = inv.getFilmByTitle(title);
    rental.addFilmToRental(film, 2);

    int result = rental.returnFilm(film, -6);

    assertEquals(-1, result);
    assertTrue(rental.getFilmsAndDays().containsKey(inv.getFilmByTitle(title)));
  }

  @Test
  public void testBonusGainedCalculation()  {
    Rental rental = new Rental(inv, user);

    rental.addFilmToRental(inv.getFilmByTitle("Baby driver"), 3);
    rental.addFilmToRental(inv.getFilmByTitle("Terminator"), 3);
    rental.addFilmToRental(inv.getFilmByTitle("Jaws"), 3);

    int bonuspointsGained = rental.getBonusPointsFromRental();
    assertEquals(4, bonuspointsGained);
  }

  @Test
  public void testBonusPurchase() {
    Rental rental = new Rental(inv, user);
    user.setBonusPoints(25);

    rental.addFilmToRental(inv.getFilmByTitle("Baby driver"), 3);
    rental.addFilmToRental(inv.getFilmByTitle("Terminator"), 3);

    int totalPrice = rental.rentAll(true);

    assertEquals(3, totalPrice);
    assertEquals(3, user.getBonusPoints());
  }

  @Test
  public void testBonusPurchaseWithNegativePoints() {
    Rental rental = new Rental(inv, user);
    user.setBonusPoints(-25);

    rental.addFilmToRental(inv.getFilmByTitle("Baby driver"), 3);
    rental.addFilmToRental(inv.getFilmByTitle("Terminator"), 3);

    assertEquals(15, rental.rentAll(true));
    assertEquals(3, user.getBonusPoints());
  }

  @Test
  public void testTotalCalulation() {
    Rental rental = new Rental(inv, user);
    user.setBonusPoints(25);

    rental.addFilmToRental(inv.getFilmByTitle("Baby driver"), 3);
    rental.addFilmToRental(inv.getFilmByTitle("Terminator"), 3);

    assertEquals(15, rental.rentAll(false));
    assertEquals(28, user.getBonusPoints());
  }
}
