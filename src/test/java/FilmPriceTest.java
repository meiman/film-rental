import com.filmrental.Film;
import com.filmrental.NewFilm;
import com.filmrental.OldFilm;
import com.filmrental.RegularFilm;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FilmPriceTest {
  @Test
  public void testNewFilmPrice()  {
    Film film = new Film("Baby driver", new NewFilm());
    assertEquals(12, film.getRentalPrice(3));
    assertEquals(0, film.getRentalPrice(-1));
  }

  @Test
  public void testRegularPrice()  {
    Film film = new Film("Lion king", new RegularFilm());
    assertEquals(3, film.getRentalPrice(2));
    assertEquals(3, film.getRentalPrice(3));
    assertEquals(9, film.getRentalPrice(5));
    assertEquals(0, film.getRentalPrice(0));
  }

  @Test
  public void testOldFilmPrice()  {
    Film film = new Film("Pulp fiction", new OldFilm());
    assertEquals(3, film.getRentalPrice(3));
    assertEquals(3, film.getRentalPrice(5));
    assertEquals(6, film.getRentalPrice(6));
    assertEquals(0, film.getRentalPrice(-1231314));
  }
}
